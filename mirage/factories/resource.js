/**
 * This file is used to create mock data representing the resource information
 * expected to be returned by the API. It will not be used on the production site.
 */

 /* global moment */

import { Factory } from 'ember-cli-mirage';

// TODO: properly implement branches
export default Factory.extend({
  name(i) {
    return `TRL 3D Printer #${i + 1}`;
  },
  facility: 'Toronto Reference Library',
  type: 'printer3d',
  requirements: [
    'GOOD_STANDING',
    '3D_PRINTER_CERTIFIED'
  ],
  description: 'This is a 3D printer',
  availableTimes() {
    return [0, 1, 2, 3, 4].map((d) => {
      let openTime = moment('2016-01-08').add(d, 'days');
      let closeTime = moment(openTime);

      let isSaturday = openTime.day() === 6;
      let isSunday = openTime.day() === 0;

      // hours: M-F 9am-8:30pm, Sa 9am-5pm, Su 1:30-5pm
      // each resource available in 15-min intervals
      openTime.set({
        hour: isSunday ? 13 : 9,
        minute: isSunday ? 30 : 0,
        second: 0,
        millisecond: 0
      });
      closeTime.set({
        hour: (isSaturday || isSunday) ? 17 : 20,
        minute: (isSaturday || isSunday) ? 0 : 30,
        second: 0,
        millisecond: 0
      });

      let times = [];
      while(closeTime.diff(openTime, 'minutes') > 0) {
        times.pushObject(openTime.format('HH:mm:ssZZ'));
        openTime.add(15, 'minutes');
      }

      return {
        date: openTime.format('YYYY-MM-DD'),
        times
      };
    });
  }
});
