import { test } from 'qunit';
import moduleForAcceptance from 'digital-booking/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | Booking');

test('Make a booking for 3D printer', function(assert) {
  assert.expect(4);

  server.createList('resource', 1);

  visit('/');

  andThen(function() {
    assert.equal(currentURL(), '/');

    // select 3D printers
    click('.equipment-selector a:first');
  });

  andThen(function() {
    assert.equal(currentURL(), '/resources?type=printer3d');

    click('.timeslots button[data-time="10:00:00"] a');
  });

  andThen(function() {
    assert.ok(currentURL().match(/^\/resources\/1\/book/), 'Should be at correct URL for booking a resource');

    fillIn('input.card-number', '12345678901234');

    click('button.submit');
  });

  andThen(function() {
    // booking created
    assert.equal(currentURL(), '/bookings/1', 'Should be showing a saved booking');
  });
});

test('Make a booking on a specific date', function(assert) {
  assert.expect(6);

  server.createList('resource', 1);

  visit('/');

  andThen(function() {
    assert.equal(currentURL(), '/');

    // select 3D printers
    click('.equipment-selector a:first');
  });

  andThen(function() {
    assert.equal(currentURL(), '/resources?type=printer3d');

    click('.resource-day-selector .date-option[data-date="2016-01-11"]');
  });

  andThen(function() {
    assert.equal(currentURL(), '/resources?type=printer3d');

    click('.timeslots button[data-time="10:45:00"] a');
  });

  andThen(function() {
    assert.ok(currentURL().match(/^\/resources\/1\/book/), 'Should be at correct URL for booking a resource');
    assert.equal(find('.booking-info .date .value').attr('data-date'), '2016-01-11',
      'Correct selected date should be shown');

    fillIn('input.card-number', '12345678901234');

    click('button.submit');
  });

  andThen(function() {
    // booking created
    assert.equal(currentURL(), '/bookings/1', 'Should be showing a saved booking');
  });
});
