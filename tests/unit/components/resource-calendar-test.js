import { moduleForComponent, test } from 'ember-qunit';
import moment from 'moment';
import Ember from 'ember';

moduleForComponent('resource-calendar', 'Unit | Component | resource calendar', {
  // Specify the other units that are required for this test
  // needs: ['component:foo', 'helper:bar'],
  unit: true
});

test('#timeslots returns appropriate values based on resource.availableTimes and date', function(assert) {
  assert.expect(2);

  let component = this.subject({
    date: moment('2017-02-14'),
    resource: Ember.Object.create({
      availableTimes: [
        {
          date: '2017-02-13',
          times: []
        },
        {
          date: '2017-02-14',
          times: [
            '12:15:00-0500',
            '12:30:00-0500',
            '12:45:00-0500',
            '13:00:00-0500'
          ]
        }
      ]
    })
  });

  let timeslots = component.get('timeslots');
  assert.equal(timeslots.length, 4);

  // 12:15pm EST == 5:15pm GMT
  assert.equal(Ember.get(timeslots, 'firstObject.time').toISOString(), '2017-02-14T17:15:00.000Z');
});
