import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import moment from 'moment';

moduleForComponent('resource-calendar', 'Integration | Component | resource calendar', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  let date = moment();
  this.setProperties({
    date,
    resource: {
      availableTimes: [{
        date: date.format('YYYY-MM-DD'),
        times: ['09:00:00-0500']
      }]
    }
  });

  this.render(hbs`{{resource-calendar date=date resource=resource}}`);

  assert.notEqual(this.$().text().trim(), '');
});
