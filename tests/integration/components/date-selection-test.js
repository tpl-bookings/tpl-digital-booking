import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('date-selection', 'Integration | Component | date selection', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(2);
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('selection', '2016-12-03');

  this.render(hbs`{{date-selection selection=selection}}`);

  assert.equal(this.$('h2').text().trim(), 'Dec 3');
  assert.equal(this.$('p').text().trim(), 'Sat');
});
