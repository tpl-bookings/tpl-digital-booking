import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import moment from 'moment';

moduleForComponent('resource-calendar-timeslot', 'Integration | Component | resource calendar timeslot', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('timeslot', {
    time: moment().set({
          hour: 12,
          minute: 34,
          second: 56,
          millisecond: 789
        })
  });

  this.render(hbs`{{resource-calendar-timeslot timeslot=timeslot}}`);

  assert.equal(this.$().text().trim(), '12:34 PM');
});
