import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('resources', function() {
    this.route('book', {path: ':resourceId/book'});
  });

  this.route('bookings', function() {
    this.route('show', {path: ':bookingId'});
  });
});

export default Router;
