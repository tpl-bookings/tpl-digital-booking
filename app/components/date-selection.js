import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  attributeBindings: ['dataDate:data-date'],
  classNames: ['date-option', 'col-xs-2', 'text-center', 'well', 'well-lg'],
  classNameBindings: ['isSelected:selected'],

  dataDate: Ember.computed('selection', function() {
    return moment(this.get('selection')).format('YYYY-MM-DD');
  }),

  isSelected: Ember.computed('selection', 'selectedDate', function() {
    let selection = this.get('selection');
    let selectedDate = this.get('selectedDate');

    return moment(selection).format('YYYYMMDD') ===
      moment(selectedDate).format('YYYYMMDD');
  }),

  click() {
    this.get('onClick')();
  }
});
