import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  tagName: 'article',
  classNames: ['resource-calendar'],

  timeslots: Ember.computed('resource.availableTimes', 'date', function() {
    let slots = [];

    let allTimeslots = this.get('resource.availableTimes') || [];

    let date = this.get('date').format('YYYY-MM-DD');
    let dateSlotsObject = allTimeslots.findBy('date', date);
    if(Ember.isEmpty(dateSlotsObject)) {
      dateSlotsObject = Ember.get(allTimeslots, 'firstObject');
      date = Ember.get(dateSlotsObject, 'date');
    }

    if(Ember.isPresent(dateSlotsObject)) {
      slots = Ember.get(dateSlotsObject, 'times').map((time) => {
        return {time: moment(`${date}T${time}`)};
      });
    }

    return slots;
  })
});
