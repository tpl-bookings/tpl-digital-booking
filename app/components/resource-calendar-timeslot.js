import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  tagName: 'button',
  attributeBindings: ['type', 'dataTime:data-time'],

  type: 'button',
  classNames: 'btn btn-default btn-lg',

  timeFormat: 'h:mm A',

  dataTime: Ember.computed('timeslot.time', function() {
    return moment(this.get('timeslot.time')).format('hh:mm:ss');
  }),

  isoFormatTime: Ember.computed('timeslot.time', function() {
    return moment(this.get('timeslot.time')).format();
  })
});
