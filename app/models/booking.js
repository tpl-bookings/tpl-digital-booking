import DS from 'ember-data';
const {attr, belongsTo, Model} = DS;

export default Model.extend({
  resource: belongsTo('resource'),
  startTime: attr('date'),
  endTime: attr('date'),
  userId: attr('string'),
  userName: attr('string'),
  email: attr('string'),
  phoneNumber: attr('string'),
  description: attr('string')

});
