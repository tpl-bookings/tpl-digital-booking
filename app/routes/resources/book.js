import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return Ember.RSVP.hash({
      booking: this.store.createRecord('booking'),
      resource: this.store.find('resource', params.resourceId)
    });
  },

  setupController(controller, resolvedModel) {
    controller.set('resource', resolvedModel.resource);
    return this._super(controller, resolvedModel.booking);
  }
});
