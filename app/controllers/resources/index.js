import Ember from 'ember';
import moment from 'moment';

export default Ember.Controller.extend({
  queryParams: ['type'],
  type: null,

  formattedType: Ember.computed('type', function() {
    // TODO: actual i18n or something
    let type = this.get('type');
    if(type === 'printer3d') {
      return '3D Printers';
    } else {
      return type;
    }
  }),

  today: Ember.computed(function() {
    return moment();
  }).readOnly(),

  date: Ember.computed(function() {
    return moment();
  }),

  dateSelections: Ember.computed('model.firstObject', function() {
    let availableTimes = this.get('model.firstObject.availableTimes');
    return availableTimes.mapBy('date').map((date) => {
      return moment(date);
    });
  }),

  actions: {
    setDate(newDate) {
      this.set('date', newDate);
    }
  }
});
